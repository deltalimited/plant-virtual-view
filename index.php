<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <!-- bootstrap css -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href="assets/front_end/css/mystyle.css" rel="stylesheet">

</head>
<body>
    <section id="main-content">
        <div class="center-block">
            <div class="btn-group-vertical" role="group" aria-label="Button group with nested dropdown">
                    <a class="btn btn-outline-primary" href="#">View of Demo1</a>
                    <a class="btn btn-outline-primary" href="#">View of Demo2</a>
                    <a class="btn btn-outline-primary" href="#">View of Demo3</a>
                    <a class="btn btn-outline-primary" href="#">View of Demo4</a>
            </div>
        </div>
    </section>

    <!-- bootstrap javascript and popper bundle -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>